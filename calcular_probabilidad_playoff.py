import random
import csv

clasificacion = [
    {"equipo": "Aniquiladores FC", "PTS": 23},
    {"equipo": "Porcinos FC", "PTS": 21},
    {"equipo": "Xbuyer Team", "PTS": 20},
    {"equipo": "Pio FC", "PTS": 19},
    {"equipo": "El Barrio", "PTS": 17},
    {"equipo": "1K FC", "PTS": 16},
    {"equipo": "Rayo de Barcelona", "PTS": 15},
    {"equipo": "Ultimate Móstoles", "PTS": 15},
    {"equipo": "Los Troncos FC", "PTS": 14},
    {"equipo": "Kunisports", "PTS": 13},
    {"equipo": "Jijantes FC", "PTS": 11},
    {"equipo": "Saiyans FC", "PTS": 8}
]

partidos_restantes = {
    "Aniquiladores FC": ["Porcinos FC", "Pio FC", "Los Troncos FC"],
    "Porcinos FC": ["1K FC", "Rayo de Barcelona"],
    "Xbuyer Team": ["1K FC", "Los Troncos FC", "Jijantes FC"],
    "Pio FC": ["Ultimate Móstoles", "Saiyans FC"],
    "El Barrio": ["Saiyans FC", "Rayo de Barcelona", "Kunisports"],
    "1K FC": ["Xbuyer Team", "Ultimate Móstoles"],
    "Rayo de Barcelona": ["Jijantes FC", "El Barrio"],
    "Ultimate Móstoles": ["Jijantes FC", "1K FC"],
    "Los Troncos FC": ["Kunisports", "Xbuyer Team", "Aniquiladores FC"],
    "Kunisports": ["Los Troncos FC", "Saiyans FC"],
    "Jijantes FC": ["Rayo de Barcelona", "Ultimate Móstoles"],
    "Saiyans FC": ["El Barrio", "Kunisports", "Pio FC"]
}

tops = [1, 4, 10]  # Tops para los que se calcularán las probabilidades

# Inicializar diccionario para almacenar los resultados de las simulaciones
resultados = {equipo["equipo"]: [0] * len(tops) for equipo in clasificacion}

num_simulaciones = 100000

for _ in range(num_simulaciones):
    # Crear una copia de la clasificación actual
    clasificacion_actual = clasificacion.copy()

    # Simular los partidos restantes
    for equipo, partidos in partidos_restantes.items():
        for rival in partidos:
            resultado = random.choice(["victoria", "derrota", "empate"])
            if resultado == "victoria":
                clasificacion_actual = [eq if eq["equipo"] != equipo else {"equipo": eq["equipo"], "PTS": eq["PTS"] + 3} for eq in clasificacion_actual]
            elif resultado == "derrota":
                clasificacion_actual = [eq if eq["equipo"] != equipo else {"equipo": eq["equipo"], "PTS": eq["PTS"] + 0} for eq in clasificacion_actual]
            else:  # empate
                ganador_penaltis = random.choice([equipo, rival])
                clasificacion_actual = [eq if eq["equipo"] != equipo and eq["equipo"] != rival else {"equipo": eq["equipo"], "PTS": eq["PTS"] + (2 if eq["equipo"] == ganador_penaltis else 1)} for eq in clasificacion_actual]

    # Ordenar la clasificación por puntos
    clasificacion_actual = sorted(clasificacion_actual, key=lambda eq: eq["PTS"], reverse=True)

    # Actualizar los resultados para cada top
    for i, top in enumerate(tops):
        equipos_top = [eq["equipo"] for eq in clasificacion_actual[:top]]
        for equipo in clasificacion:
            if equipo["equipo"] in equipos_top:
                resultados[equipo["equipo"]][i] += 1

# Calcular las probabilidades
probabilidades = {equipo: [round(resultados[equipo][i] / num_simulaciones * 100, 2) for i in range(len(tops))] for equipo in resultados}

# Preparar los datos para el archivo CSV
data = [["Equipo"] + ["Probabilidad Top {}".format(top) for top in tops]]
for equipo in clasificacion:
    row = [equipo["equipo"]] + probabilidades[equipo["equipo"]]
    data.append(row)

# Exportar los resultados en un archivo CSV
with open("resultados.csv", "w", newline="") as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(data)